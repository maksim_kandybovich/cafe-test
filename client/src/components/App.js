import React from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components';
import Header from './Header';
import DishesContainer from './Dishes/DishesContainer';
import Contacts from './Contacts';
import CartContainer from './Cart/CartContainer';
import { Container } from './Theme/Theme';

const App = () =>
  <Wrapper>
    <Header />
    <AppContainer>
      <Pages>
        <Route exact path="/" component={ DishesContainer } />
        <Route path="/contacts" component={ Contacts } />
        <Route path="/cart" component={ CartContainer } />
      </Pages>
      <SideBar>
        <CartContainer />
      </SideBar>
    </AppContainer>
  </Wrapper>;

export default App;

const Wrapper = styled.div`
  padding-bottom: 40px;
`;

const AppContainer = styled(Container)`
  display: flex;
  padding-top: 40px;
  padding-bottom: 40px;
`;

const Pages = styled.div`
  width: 100%;
`;

const SideBar = styled.aside`
  display: none;
  @media (min-width: 768px) {
    display: block;
    padding-left: 20px;
    flex: 0 0 300px;
  }
`;
