import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Delete } from '../Theme/Icons';
import Theme from '../Theme/Theme';

const Cart = ({ items = [], total = 0, removeFromCart }) =>
  <CartWrapper>
    <Title>Cart</Title>
    <List>
      {
        items.map(({ menuItem, price, id }) =>
          <ListItem key={ id }>
            <div>{ menuItem }</div>
            <PriceWrap>
              ${ price }
              <IconWrapper onClick={ () => removeFromCart(id) }>
                <Delete color={ Theme.colors.primary } />
              </IconWrapper>
            </PriceWrap>
          </ListItem>)
      }
    </List>
    <TotalWrap>
      Total: ${ total }
    </TotalWrap>
  </CartWrapper>;

Cart.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})),
  removeFromCart: PropTypes.func,
  total: PropTypes.number,
};

export default Cart;

const IconWrapper = styled.div`
  position: relative;
  top: 2px;
  cursor: pointer;
`;

const CartWrapper = styled.div`
  background-color: #f9f9f9;
  border-radius: 4px;
  box-sizing: border-box;
  padding: 20px;
`;

const Title = styled.h3`
  margin: 0 0 20px;
`;

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;

const ListItem = styled.li`
  display: flex;
  justify-content: space-between;
`;
const PriceWrap = styled.div`
  padding-left: 10px;
  display: flex;
  align-items: center;
`;
const TotalWrap = styled.div`
  padding-top: 20px;
  margin-top: 20px;
  border-top: 1px double #ebe9eb;
  text-align: right;
`;
