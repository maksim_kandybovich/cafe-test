import { connect } from 'react-redux';
import { removeFromCart } from '../../reducers/cart';
import Cart from './Cart';

const mapStateToProps = ({ cart: { items, total } }) => ({
  items,
  total,
});

export default connect(mapStateToProps, { removeFromCart })(Cart);
