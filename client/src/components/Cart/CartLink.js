import React from 'react';
import { Route, Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';
import Theme from '../Theme/Theme';
import { connect } from 'react-redux';

const CartLink = ({
  activeOnlyWhenExact,
  label,
  to,
  total,
}) => (
  <Route
    path={ to }
    exact={ activeOnlyWhenExact }
    children={ ({ match }) => (
      <MenuLink to={ to } match={ match }>
        <span>{ label }</span>
        <CartTotal>${ total }</CartTotal>
      </MenuLink>
    ) } />
);

CartLink.propTypes = {
  activeOnlyWhenExact: PropTypes.bool,
  label: PropTypes.string,
  to: PropTypes.string,
  total: PropTypes.number,
};

const mapStateToProps = ({
  cart: { total },
}) => ({
  total,
});

export default connect(mapStateToProps, {})(CartLink);

const MenuLink = styled(Link)`
  color: #fff;
  text-decoration: none;
  margin: 10px;
  opacity: 1;
  font-size: 20px;
  transition: all .3s ease-in;
  position: relative;
  @media (min-width: 768px) {
    display: none;
  }
  ${props => props.match && css`
    opacity: 1;
  `}
`;

const CartTotal = styled.span`
  color: #fff;
  border-radius: 10px;
  font-size: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${Theme.colors.primary};
  position: absolute;
  right: -4px;
  top: 0;
  min-width: 17px;
  padding: 0 3px;
  transform: translateX(100%);
`;

