import React from 'react';
import { Route, Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

const CustomLink = ({ label, to, activeOnlyWhenExact }) => (
  <Route
    path={ to }
    exact={ activeOnlyWhenExact }
    children={ ({ match }) => (
      <MenuLink to={ to } match={ match }>{ label }</MenuLink>
    ) } />
);

CustomLink.propTypes = {
  activeOnlyWhenExact: PropTypes.bool,
  label: PropTypes.string,
  to: PropTypes.string,
};

export default CustomLink;

const MenuLink = styled(Link)`
  color: #fff;
  text-decoration: none;
  margin: 10px;
  opacity: 1;
  font-size: 20px;
  transition: all .3s ease-in;
  @media (min-width: 768px) {
  }
  ${props => props.match && css`
    @media (min-width: 768px) {
        opacity: 1;
    }
  `}

`;
