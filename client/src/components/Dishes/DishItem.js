import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import Grill from '../../Wasabi-Wednesday.jpg';
import Pasta from '../../Mango-Chile-Chutney.jpg';
import Deli from '../../Carne-Asada-Tacos.jpg';

const DishItem = ({ addItemToCart, item }) =>
  <Item>
    <Image itemType={ item.itemType } />
    <Description>
      <Title>{ item.menuItem }</Title>
      <Category>{ item.itemType }</Category>
      ${ item.price }
      <AddToCart onClick={ addItemToCart(item) }>Add to cart</AddToCart>
    </Description>
  </Item>;

DishItem.propTypes = ({
  addItemToCart: PropTypes.func,
  item: PropTypes.shape({}),
});

export default DishItem;

const imagesMap = {
  Grill,
  Pasta,
  Deli,
};

const Item = styled.div`
  width: 100%;
  margin: 0 0 20px 20px;
  background-color: #f9f9f9;
  border-radius: 4px;
  box-sizing: border-box;
  padding-bottom: 40px;
  position: relative;
  overflow: hidden;
  transition: box-shadow 0.3s cubic-bezier(.21, .6, .35, 1);
  &:hover {
    box-shadow: 0 10px 35px rgba(0,0,0,0.17);
  }
  @media (min-width: 576px) {
    width: calc(100% / 2 - 20px);
  }
  @media (min-width: 992px) {
    width: calc(100% / 3 - 20px);
  }
`;

const Image = styled.div`
  ${({ itemType }) => css`
    background-image: url(${imagesMap[itemType]});
  `}
  background-size: cover;
  padding-top: 70%;
`;

const Description = styled.div`
  padding: 10px;
`;

const Title = styled.h3`
  margin: 0;
  font-size: 16px;
  lin-height: 20px;
`;
const Category = styled.div`
  margin: 0 0 10px;
  font-size: 12px;
  lin-height: 20px;
  color: #8a8a8a;
`;

const AddToCart = styled.button`
  position: absolute;
  width: calc(100% - 20px);
  bottom: 10px;
  left: 10px;
  padding: 8px 13px;
  background: transparent;
  font-size: 12px;
  line-height: 12px;
  display: inline-block;
  transition: color 0.2s linear;
  border: 1px solid rgba(0,0,0,0.13);
  cursor: pointer;
  &:hover {
    color: rgb(63, 89, 255);
  }
`;
