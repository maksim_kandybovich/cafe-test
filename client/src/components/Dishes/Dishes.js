import React from 'react';
import PropTypes from 'prop-types';
import FilterContainer from '../Filter/FilterContainer';
import DishItem from './DishItem';
import styled from 'styled-components';

const Dishes = ({
  items,
  isFetching,
  isFetched,
  addItemToCart,
}) =>
  <div>
    <FilterContainer />
    { isFetched && !items.length && <h4>Nothing found =(</h4> }
    {
        isFetching ?
          <h2>Loading...</h2> :
          <Wrapper>
            {
              items.map((item, index) =>
                <DishItem key={ index } addItemToCart={ addItemToCart } item={ item } />)
            }
          </Wrapper>
      }
  </div>;

Dishes.propTypes = ({
  addItemToCart: PropTypes.func,
  isFetched: PropTypes.bool,
  isFetching: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.shape({})),
});

export default Dishes;

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: -20px;
  padding-top: 20px;
`;
