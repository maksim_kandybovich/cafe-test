import { connect } from 'react-redux';
import { compose, lifecycle, withHandlers } from 'recompose';
import Dishes from './Dishes';
import { loadMenu } from '../../reducers/dishes';
import { addToCart } from '../../reducers/cart';

const mapStateToProps = ({ dishes: { sortedItems, isFetched, isFetching } }) => ({
  items: sortedItems,
  isFetched,
  isFetching,
});

export default compose(
  connect(mapStateToProps, { loadMenu, addToCart }),
  lifecycle({
    componentDidMount() {
      const { isFetched, loadMenu } = this.props;
      if (!isFetched) {
        loadMenu();
      }
    },
  }),
  withHandlers({
    addItemToCart: ({ addToCart }) => item => () => {
      addToCart(item);
    },
  }),
)(Dishes);
