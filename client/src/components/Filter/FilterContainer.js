import { connect } from 'react-redux';
import { compose, withHandlers } from 'recompose';
import { filterItems, filterItemsByType } from '../../reducers/dishes';
import Filters from './Filters';

const mapStateToProps = ({
  dishes: { types = [], isFetching, filters },
}) => ({
  types,
  isFetching,
  filters,
});

export default compose(
  connect(mapStateToProps, { filterItems, filterItemsByType }),
  withHandlers({
    setFilter: ({ filterItems }) => (type, value) => {
      filterItems(type, value);
    },
    setFilterItemsByType: ({ filterItemsByType }) => (type) => {
      filterItemsByType(type);
    },
  }),
)(Filters);
