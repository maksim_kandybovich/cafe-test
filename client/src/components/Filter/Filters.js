import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import Theme from '../Theme/Theme';
import { ArrowDown } from '../Theme/Icons';

const Filters = ({
  types,
  isFetching,
  setFilter,
  filters = {},
  setFilterItemsByType,
}) =>
  <Wrapper>
    {
      types.map((type, index) =>
        <FilterItem
          isActive={ filters.byCategory[type] }
          key={ index }
          onClick={ () => setFilterItemsByType(type) }>
          { type}
        </FilterItem>)
    }
    <FilterItem
      isActive={ filters.byTitle }
      onClick={ () => setFilter('byTitle') }>
      Sort by title
      <FilterIcon
        isActive={ filters.byTitle }
        color={ filters.byTitle ? Theme.colors.primary : null } />
    </FilterItem>
    <FilterInput
      type="text"
      placeholder="Type name to search..."
      onChange={ ({ target: { value } }) => setFilter('byText', value) } />
  </Wrapper>;

Filters.propTypes = ({
  filters: PropTypes.shape({}),
  isFetching: PropTypes.bool,
  setFilter: PropTypes.func,
  setFilterItemsByType: PropTypes.func,
  types: PropTypes.arrayOf(PropTypes.string),
});

export default Filters;

const FilterItem = styled.button`
  background: transparent;
  cursor: pointer;
  border: none;
  display: inline-flex;
  align-items: center;
  line-height: 25px;
  transition: color .3s ease-in;
  outline: none;
  border-bottom: 1px solid transparent;
  &:hover {
    color: ${Theme.colors.primary};
  }
  ${props => props.isActive && css`
    color: ${Theme.colors.primary};
    border-bottom: 1px solid ${Theme.colors.primary};
  `}
  
`;
const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  @media (min-width: 768px) {
    flex-wrap: nowrap;
  }
`;

const FilterIcon = styled(ArrowDown)`
  ${props => !props.isActive && css`
    transform: rotateZ(180deg);
  `}
`;

const FilterInput = styled.input`
  border-width: 0 0 1px 0;
  border-style: solid;
  border-color: transparent;
  transition: all .3s ease-in;
  padding: 5px;
  width: 100%;
  margin-top: 20px;
  height: 25px;
  &:hover {
    border-color: ${Theme.colors.primary};
  }
  @media (min-width: 768px) {
    width: auto;
    margin-top: 0;
  }
`;
