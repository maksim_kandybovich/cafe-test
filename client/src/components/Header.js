import React from 'react';
import styled from 'styled-components';
import bgImage from '../home-5-slide-32.jpg';
import Navigation from './Navigation';
import { Container } from './Theme/Theme';

const Header = () =>
  <Wrapper>
    <Navigation />
    <Container>
      <HeaderTitle>Welcome to Cafe</HeaderTitle>
    </Container>
  </Wrapper>;

export default Header;

const Wrapper = styled.header`
  background-image: url(${bgImage});
  background-size: cover;
  text-align: center;
  color: #FFF;
`;

const HeaderTitle = styled.h1`
  margin: 0;
  font-size: 30px;
  padding: 20px 0 40px;
`;
