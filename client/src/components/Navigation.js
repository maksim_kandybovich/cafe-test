import React from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';
import { lifecycle, compose, withStateHandlers } from 'recompose';
import CustomLink from './CustomLink';
import CartLink from './Cart/CartLink';
import Theme from './Theme/Theme';

const Navigation = ({ isFixed }) =>
  <Menu>
    <MenuInner isFixed={ isFixed }>
      <CustomLink activeOnlyWhenExact to="/" label="Menu" />
      <CustomLink activeOnlyWhenExact to="/contacts" label="Contacts" />
      <CartLink activeOnlyWhenExact to="/cart" label="Cart" />
    </MenuInner>
  </Menu>;

Navigation.propTypes = {
  isFixed: PropTypes.bool,
};

export default compose(
  withStateHandlers(
    () => ({ isFixed: false }),
    {
      makeSticky: ({ isFixed }) => () => {
        if (window.pageYOffset >= 0 && !isFixed) {
          return {
            isFixed: true,
          };
        } else if (window.pageYOffset === 0 && isFixed) {
          return {
            isFixed: false,
          };
        }
      },
    },
  ),
  lifecycle({
    componentDidMount() {
      document.addEventListener('scroll', this.props.makeSticky, false);
    },

    componentWillUnmount() {
      document.removeEventListener('scroll', this.props.makeSticky, false);
    },
  }),
)(Navigation);

const Menu = styled.div`
  min-height: 70px;
`;

const MenuInner = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  z-index: 10;
  left: 0;
  top: 0;
  width: 100%;
  transition: all .3s ease-in;
  padding: 10px 0;
  ${props => props.isFixed && css`
    background: #fff;
    position: fixed;
    box-shadow: 0 0 3px 0 rgba(0,0,0,0.22);
    & a {
      color: ${Theme.colors.primary};
    }
  `}
`;
