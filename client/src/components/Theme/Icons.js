import React from 'react';
import Theme from './Theme';
import PropTypes from 'prop-types';

export const ArrowDown = ({ color = Theme.colors.black, size = 16, className }) =>
  <svg fill={ color } className={ className } height={ size } viewBox="0 0 24 24" width={ size } xmlns="http://www.w3.org/2000/svg">
    <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
    <path d="M0-.75h24v24H0z" fill="none" />
  </svg>;

ArrowDown.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.number,
};

export const Delete = ({ color = Theme.colors.black, size = 16, className }) =>
  <svg fill={ color } className={ className } height={ size } viewBox="0 0 24 24" width={ size } xmlns="http://www.w3.org/2000/svg">
    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
    <path d="M0 0h24v24H0z" fill="none" />
  </svg>;

Delete.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.number,
};

