import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  max-width: 1140px;
  box-sizing: border-box;
  @media (min-width: 576px) {
     max-width: 540px;
  }
  @media (min-width: 768px) {
    max-width: 720px;
  }
  @media (min-width: 992px) {
    max-width: 960px;
  }
`;

const colors = {
  black: '#212529',
  white: '#ffffff',
  primary: '#3e58ff',
};

export default {
  colors,
};
