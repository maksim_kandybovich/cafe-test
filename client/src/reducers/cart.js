import { createReducer, makeActionCreator } from '../utils';

const ADD_TO_CART = 'ADD_TO_CART';
const REMOVE_FROM_CART = 'REMOVE_FROM_CART';

export const addToCart = makeActionCreator(ADD_TO_CART, 'payload');
export const removeFromCart = makeActionCreator(REMOVE_FROM_CART, 'payload');

const initialState = {
  items: [],
  total: 0,
};

const calculateTotal = items => +items.reduce((prev, { price }) => (prev + price), 0).toFixed(10);

const cartReducer = createReducer(initialState, {
  [ADD_TO_CART]: (state, { payload }) => {
    const item = { ...payload, ...{ id: Number(Date.now()) } };
    const total = state.total + parseFloat(item.price);
    return {
      ...state,
      items: [...state.items, item],
      total: +total.toFixed(10),
    };
  },
  [REMOVE_FROM_CART]: (state, { payload }) => {
    const items = [...state.items].filter(item => item.id !== payload);
    return {
      ...state,
      items,
      total: calculateTotal(items),
    };
  },
});

export default cartReducer;
