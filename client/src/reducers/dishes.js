import {
  createReducer,
  makeActionCreator,
  extractTypes,
  compose,
  typesToObj,
  filterFunctions,
} from '../utils';
import { CALL_API } from '../middleware/callApi';

const url = 'http://www.mocky.io/v2/5a8dd7342f00005a004f2509';

const LOAD_MENU_REQUEST = 'LOAD_MENU_REQUEST';
const LOAD_MENU_SUCCESS = 'LOAD_MENU_SUCCESS';
const LOAD_MENU_FAILURE = 'LOAD_MENU_FAILURE';

const FILTER_ITEMS = 'FILTER_ITEMS';
const FILTER_ITEMS_BY_TYPE = 'FILTER_ITEMS_BY_TYPE';

export const filterItems = makeActionCreator(FILTER_ITEMS, 'filterType', 'value');
export const filterItemsByType = makeActionCreator(FILTER_ITEMS_BY_TYPE, 'filterType');

export const loadMenu = () => ({
  [CALL_API]: {
    types: [LOAD_MENU_REQUEST, LOAD_MENU_SUCCESS, LOAD_MENU_FAILURE],
    method: 'GET',
    endpoint: url,
  },
});

const initialState = {
  items: [],
  sortedItems: [],
  isFetched: false,
  isFetching: false,
  error: null,
  types: [],
  filters: {
    byText: '',
    byTitle: false,
    byCategory: {},
  },
};

const applyFilters = (items, filters) => {
  const func = Object.keys(filters).map((filterKey) =>
    filterFunctions[filterKey].bind(null, filters[filterKey]));

  return compose(func)(items);
};

const menuSuccess = (state, { response }) => {
  const types = extractTypes(response);
  return {
    ...state,
    items: response,
    sortedItems: response,
    types,
    filters: {
      ...state.filters,
      byCategory: typesToObj(types),
    },
    isFetching: false,
    isFetched: true,
  };
};

const filterItem = (state, { filterType, value }) => {
  const filters = { ...state.filters };
  filters[filterType] = value || !filters[filterType];
  return {
    ...state,
    filters,
    sortedItems: applyFilters([...state.items], filters),
  };
};

const filterItemByType = (state, { filterType }) => {
  const filters = { ...state.filters };
  filters.byCategory[filterType] = !filters.byCategory[filterType];
  return {
    ...state,
    filters,
    sortedItems: applyFilters([...state.items], filters),
  };
};

const dishesReducer = createReducer(initialState, {
  [LOAD_MENU_REQUEST]: state => ({ ...state, isFetching: true }),
  [LOAD_MENU_SUCCESS]: menuSuccess,
  [LOAD_MENU_FAILURE]: state => ({ ...state, isFetching: false, error: true }),
  [FILTER_ITEMS]: filterItem,
  [FILTER_ITEMS_BY_TYPE]: filterItemByType,
});

export default dishesReducer;
