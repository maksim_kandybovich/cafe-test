import { combineReducers } from 'redux';
import dishesReducer from './dishes';
import cartReducer from './cart';

const rootReducer = combineReducers({
  dishes: dishesReducer,
  cart: cartReducer,
});

export default rootReducer;
