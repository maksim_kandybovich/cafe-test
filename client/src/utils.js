export const createReducer = (initialState, handlers) =>
  function reducer(state = initialState, action) {
    if (handlers.hasOwnProperty(action.type)) { // eslint-disable-line
      return handlers[action.type](state, action);
    }
    return state;
  };

export const makeActionCreator = (type, ...argNames) => function (...args) {
  const action = { type };
  argNames.forEach((arg, index) => {
    action[argNames[index]] = args[index];
  });
  return action;
};

export const extractTypes = items => items.reduce((previousValue, { itemType }) => (
  previousValue.includes(itemType) ?
    previousValue :
    [...previousValue, itemType]
), []);

export const compose = fns => x => fns.reduce((v, f) => f(v), x);

export const typesToObj = types => types.reduce(
  (prev, curr) => ({ ...prev, ...{ [curr]: false } }),
  {},
);

export const filterFunctions = {
  byText: (value, items) => {
    if (value) {
      return items.filter(item =>
        item.menuItem.toLowerCase().includes(value.toLowerCase()));
    }
    return items;
  },
  byTitle: (value, items) => items.sort((itemA, itemB) => {
    const menuItemA = itemA.menuItem.toLowerCase();
    const menuItemB = itemB.menuItem.toLowerCase();
    if (menuItemA < menuItemB) return value ? -1 : 1;
    if (menuItemA > menuItemB) return value ? 1 : -1;
    return 0;
  }),
  byCategory: (value, items) => {
    if (Object.keys(value).every(item => !value[item])) {
      return items;
    }
    return items.filter(({ itemType }) => value[itemType]);
  },
};
